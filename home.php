<?include('head.php')?>

<div class="big static left intro">
  The Gerrit Rietveld Academie is a university of applied sciences for Fine Arts and Design. We offer one Bachelor's degree programme and four Master's programmes.
  ead more about our <a href="">12 different</a> full time and part time programmes. If you are <a href="">interested in applying</a> some stuff.
</div>

<div class="editors-notice small">
  <span class="line">During 10 March - 30 March 2016</span>
  <span class="line"><a href="<?=$pages->get('/joel-galvez')->url?>">Joel&nbsp;Galvez</a> selects the projects shown on the front page. </span>

  <span class="line"><a href="#editors-note" class="">Read more</a></span>
</div>
<?
  $projects = $pages->find('template=project,limit=10,sort=-created');
?>
<?if($projects->eq(0)):?>
  <?
    $p = $projects->eq(0);
  ?>
  <?project($p,$page,$pages,['first'=>true])?>
<?endif?>
<div class="columns">
  <?
    $columns = $page->columns;
  ?>
  <?foreach($columns as $c):?>
    <div class="column staircase">
      <?=$c->text?>
    </div>
  <?endforeach?>
</div>
<?if($projects->eq(1)):?>
  <?
    $p = $projects->eq(1);
  ?>
  <?project($p,$page,$pages)?>
<?endif?>
<div class="content news linewidth">
  <div class="title">News</div>
  <ul>
    <li><span class="title"><a href="">Bots, Bodies, Beasts</a></span></li>
    <li><span class="title"><a href="">Dr A.H. Heineken Prize for Art 2016 awarded to Yvonne Dröge Wendel </a></span></li>
    <li><span class="title"><a href="">Call for applications Sandberg Instituut - Master of Voice</a></span></li>
  </ul>
  <a href="news">All news</a>
</div>
<?if($projects->eq(2)):?>
  <?
    $p = $projects->eq(2);
  ?>
  <?project($p,$page,$pages)?>
<?endif?>
<div class="content">
  <div class="calendar linewidth">
    <span class="add button">+ Add to Calendar</span>
    <div class="title">
      Calendar
    </div>

    <ul class="">
      <li>
        <span class="date">6-10 April</span>
        <span class="added-by">added by <a href="">Public Rietveld</a></span>
        <span class="title"><a href="">Bots, Bodies, Beasts | Conference-festival</a></span>
      </li>
      <li>
        <span class="date">22 March - 24 May</span>
        <span class="added-by">added by <a href="">Public Rietveld</a></span>
        <span class="title"><a href="">"8282" by Hyoseop Kim in WOW Amsterdam</a></span>
      </li>
      <li>
        <span class="date">Sunday March 20</span>
        <span class="added-by">added by <a href="">Public Rietveld</a></span>
        <span class="title"><a href="">Café Chercher at the Rietveld Academie's glass workshop </a></span>
      </li>
      <li>
        <span class="date">1 April 17:00</span>
        <span class="added-by">added by <a href="">Public Rietveld</a></span>
        <span class="title"><a href="">OPENING Inter-Architectyre presents 'Modernizing MODERNISM' in Van Eesteren Museum</a></span>
      </li>
      <li>
        <span class="date">1 April - 1 June</span>
        <span class="added-by">added by <a href="">Public Rietveld</a></span>
        <span class="title"><a href="">Inter-Architectyre presents 'Modernizing MODERNISM' in Van Eesteren Museum</a></span>
      </li>
      <!-- <li>
        <span class="date">March 7 – 18</span>
        <span class="added-by">added by <a href="">Public Rietveld</a></span>
        <span class="title"><a href="">Rietveld Pavilion Residency: 'Preparing to Give Up. Invitation to. Contribute Learning to' by Alexandra Symons Sutcliffe, Phoebe Eustance & Laurie Robins, Centre for Research Architecture, Goldsmiths (UK) Inter-Architectyre presents 'Modernizing MODERNISM' in Van Eesteren Museum</a></span>
      </li>
      <li>
        <span class="date">March 7</span>
        <span class="added-by">added by <a href="">Public Rietveld</a></span>
        <span class="title"><a href="">The Library Talks #8: Emiliano Battista about 'Daan van Golden Photo Book(s)'. In collaboration with the Rietveld Academie and Sandberg Institute library </a></span>
      </li> -->
    </ul>
    <a href="calendar" class="">View full calendar</a>
  </div>
</div>
<?if($projects->eq(3)):?>
  <?
    $p = $projects->eq(3);
  ?>
  <?project($p,$page,$pages)?>
<?endif?>
<div class="columns">
  <?
    $columns = $page->columns;
  ?>
  <?foreach($columns as $c):?>
    <div class="column staircase-reversed">
      <?=$c->text?>
    </div>
  <?endforeach?>
</div>
<?if($projects->eq(4)):?>
  <?
    $p = $projects->eq(4);
  ?>
  <?project($p,$page,$pages)?>
<?endif?>
<div class="content">
  <div class="editors-notice">
    <span class="line small">During 10 March - 30 March 2016</span>
    <span class="line small">the front page is edited by <a href="<?=$pages->get('/joel-galvez')->url?>">Joel&nbsp;Galvez</a> </span>
    <blockquote class="big">
      This is a text about why i selected these projects. This is a text about why i selected these projects. This is a text about why i selected these projects.
    </blockquote>
    <div class="previous-week">
      <a href="front-page-editor/" class="button">Edit front page</a>
      <a href="">View previous front page</a>
    </div>
  </div>

</div>

<?include('foot.php')?>
