$(function() {

	// FastClick.attach($('.clickarea')[0]);
	// FastClick.attach($('.main')[0]);


	$('nav .title').click(function(e) {
		$(this).closest('.expandable').toggleClass('expanded');

	})

	$('.main').click(function() {
		positionThings();
	})


	$('.hamburger').click(function(e) {
		$('html').addClass('nav-open');
		positionThings();
	})
	$('.close').click(function(e) {
		$('html').removeClass('nav-open');
		positionThings();
	})

	$(window).resize(function() {
		positionThings();
		// $('.main').css('margin-right', menuWidth+'px');
	})

	// $('.overview .stub').click(function(e) {
	// 	var t = $(this);
	// 	$('.overview .stub').removeClass('active');
	// 	t.addClass('active');
	// 	var ww = $(window).width();
	// 	$('.overview').width(ww*5)
	// 	$('.body').delay( 300 ).fadeIn( 100 );
	//
	// 	$('.overview')[0].style.webkitTransform = 'scale(0.2)';
	// })
	//
	// $('.close').click(function() {
	// 	$('.overview .stub').removeClass('active');
	// 	$('.body').hide();
	// 	$('.overview')[0].style.webkitTransform = 'scale(1)';
	// });

	setTimeout(function() {
		positionThings();
	},0)
	$(window).trigger('resize');
});

function positionThings() {
	var oneRem = getEmPixels();
	var halfRem = Math.round(oneRem/2);
	ww = $(window).width();
	var hhGap = halfRem*1.5;
	var rightHWidth = halfRem*2;
	var sidePadding = halfRem;
	var menuWidth = $('nav').outerWidth();
	if($('html').hasClass('nav-open')) {
		translate3dX($('nav')[0], 0);
	} else {
		translate3dX($('nav')[0], menuWidth);
	}

}

function translate3dX(el, xpx, scalex) {

	if(scalex!=undefined) {
		el.style.webkitTransform = 'translate3d('+xpx+'px, 0, 0) scale3d('+scalex+', 1, 1)';
		el.style.mozTransform = 'translate3d('+xpx+'px, 0, 0) scale3d('+scalex+', 1, 1)';
		el.style.transform = 'translate3d('+xpx+'px, 0, 0) scale3d('+scalex+', 1, 1)';

	} else {
		el.style.webkitTransform = 'translate3d('+xpx+'px, 0, 0)';
		el.style.mozTransform = 'translate3d('+xpx+'px, 0, 0)';
		el.style.transform = 'translate3d('+xpx+'px, 0, 0)';

	}


}
