<?function topbar($page,$pages) {?>
	<div class="hamburger">
		Menu
	</div>
	<div class="notifications">
	 <div class="number">3</div>
	 <div class="expanded">
		 3/4 16:00 Joel Boel added an image in XYZ<br>
		 3/4 14:00 Ada Baba removed an image in XXY<br>
		 3/4 11:00 Ada Baba changed the order in XXY<br>
	 </div>
	</div>
	<div class="crumbs">
		<a class="main" href="<?=$pages->get('/')->url?>"><?=nobreak($pages->get('/')->title . '</a>')?>
		<?if($page->id!=$pages->get('/')->id):?>
			/&nbsp;<a class="sub" href="<?=$page->url?>"><?=nobreak($page->title . '</a>')?></a><!--
			--><?if(count($page->parents('template!=home'))>0):?>&nbsp;<!--
			--><span class="partof"><!--
			-->(part&nbsp;of&nbsp;<!--
					--><?foreach($page->parents as $parent):?><!--
					--><?if($parent->id==$pages->get('/')->id) continue;?><!--
					--><span class="part"><a href="<?=$parent->url?>"><?=nobreak($parent->title . '</a>')?></a></span><!--
						--><?endforeach?>)
				</span>
			<?endif?>
		<?endif?>
	</div>
<?}?>
<?
	function visit(Page $parent, $enter, $exit=null, $currentPage=null)
	{
			foreach ($parent->children() as $child) {
					call_user_func($enter, $child, $currentPage);

					if ($child->numChildren > 0) {
							visit($child, $enter, $exit, $currentPage);
					}

					if ($exit) {
							call_user_func($exit, $child, $currentPage);
					}
			}
	}
	function nobreak($str) {
		$str = str_replace('-', '&#8209;', $str);
		return str_replace(' ', '&nbsp;', $str);
	}

?>
<?function project($p,$page,$pages,$args) {?>
	<div class="project <?=$args['first']?'first':''?>">
		<?if($p->iconimage):?>
			<?
				$thumb = $p->iconimage;
				if($p->iconimage->ext!="gif") {
					$thumb = $p->iconimage->size(2000,1300,['upscaling'=>false,'cropping'=>false]);
				}
			?>
			<div class="image">
				<img src="<?=$thumb->url?>" alt="">
			</div>

			<div class="caption left">
				<div class="title"><em><?=$p->title?></em>&nbsp; <a href="<?=$p->url?>">View 4 More <span class="arrow">→</span></a></div>
			</div>
			<div class="caption right small">
				added by <a href="<?=$pages->get('/joel-galvez')->url?>">Joel Galvez</a> in <a href=""><?=$p->parent->title?></a>
			</div>
		<?elseif($p->iconvideo):?>

		<?else:?>
		<div class="title-wrapper">
			<div class="title"><?=$p->title?> </div>

		</div>
		<div class="caption left small"><a href="">Read more  <span class="arrow">→</span></a></div>
		<div class="caption right small">Added by <a href="<?=$pages->get('/joel-galvez')->url?>">Joel Galvez</a> in <a href=""><?=$p->parent->title?></a></div>
		<?endif?>
	</div>
<?}?>
<?function icon($p,$page,$pages) {?>
	<a class="icon" href="<?=$p->url?>">
		<?if($p->iconimage):?>
			<?
				$thumb = $p->iconimage;
				if($p->iconimage->ext!="gif") {
					$thumb = $p->iconimage->size(500,500,['upscaling'=>false,'cropping'=>false]);
				}
			?>
			<div class="image">
				<img src="<?=$thumb->url?>" alt="">
			</div>

			<div class="title"><?=$p->title?></div>
		<?elseif($p->iconvideo):?>

		<?else:?>
		<div class="title-wrapper">
			<div class="title"><?=$p->title?> </div>
		</div>
		<div class="title"><?=$p->title?></div>
		<?endif?>
	</a>
<?}?>


<!DOCTYPE html>
<html lang="en" class="">
<head>
	<meta charset="UTF-8">
	<title><?=$page->title?></title>
	<script src="<?=$config->urls->templates?>plugs.js"></script>
	<script src="<?=$config->urls->templates?>main.js"></script>
	<link rel="stylesheet" type="text/css" href="<?=$config->urls->templates?>reset.css">
	<link rel="stylesheet" type="text/css" href="<?=$config->urls->templates?>css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<style>
		@font-face {
			font-family: font;
			src: url('<?=$pages->get('/')->font->url;?>');
		}
		body {
			font-family:font,sans-serif;;
		}
	</style>
</head>
<body class="template-<?=$page->template->name?>">
	<div class="container">
		<div class="topbar invisible">
			 <?topbar($page,$pages)?>
		</div>
		<main>
