var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var sass = require('gulp-sass');

gulp.task('default', function () {
    return gulp.src('./scss/**/*.scss')
        .pipe(sass({
          // paths: [ path.join(__dirname, 'scss', 'includes') ]
        }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))

        .pipe(gulp.dest('./css'));
});

var watcher = gulp.watch('./scss/**/*.scss', ['default']);
