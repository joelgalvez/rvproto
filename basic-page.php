<?include('head.php');?>
<ul class="sub">
	<?
		$subs = $page->children('template=basic-page');
	?>
	<?foreach($subs as $s):?>
		<li><a href="<?=$s->url?>"><?=nobreak($s->title)?></a></li>
	<?endforeach?>
</ul>
<?if($page->sketch):?>
<div class="sketch">
	<div class="stamp">Wireframe</div>
	<img src="<?=$page->sketch->url?>" alt="">
</div>
<?else:?>
	<article>
		<div class="content auto-zigzag">
			<!-- <div class="text static small">
				<p>See also: <a href="<?=$pages->get('/workshops')->url?>">Workshops and facilities</a></p>
			</div> -->

			<div class="text static">
				<p>The aim of the Graphic Design course at the Rietveld is to develop a students understanding of the role,
					desires and possibilities of design, towards (independent) practice.
					The department focuses on teaching students how to organise and shape information within today’s social and cultural context,
					based on knowledge of the history of design and with an emphasis on the role played by conceptual and editorial approaches to it.</p>
			</div>
			<div class="small static">
				<p>Contact schroderrietveld@gmail.com<br>
				and/or visit grablog.rietveldacademie.nl<br>
				For graphic assignments contact praktijkbureau@rietveldacademie.nl, more information about: work with our students<p>
			</div>
			<div class="text static">
				<p>In the Graphic Design course you will learn how to address and analyse a question or issue and adopt your own stance towards it. Our course teaches you how to take a critical approach to content and to take responsibility for your chosen points of departure, meanwhile teaching you to give form to your ideas. Developing a personal, autonomous and curious approach to design practice is a crucial aspect of our programme. To begin to understand and focus more closely on any personal preferences you might have – by means of critical reflection – you can begin to make conscious and effective use of images and text. In the department you will have extensive opportunities to learn new techniques and have access to a range of relevant and recent media. The department encourages discussion and debate and provides a context for this by teaching relevant theories from the humanities (art, media, philosophy and semiotics, etc.).</p>
			</div>
			<div class="text static">
				<p>Towards graduation, the course aims to educate designers who can contribute to the development of the discipline, are prepared to use their work to engage in debate, adopt an investigative and reflective approach to media, and can express themselves in their own specific design language.</p>
			</div>
		</div>
	</article>
	<div class="projects">
		<div class="add-new">
			<span class="button">+ Add new project in <?=$page->title?></span>
		</div>
		<?
		  $projects = $page->children('template=project');

		?>
		<?foreach($projects as $p):?>
		  <?project($p,$page,$pages)?>
		<?endforeach?>
	</div>
	<div class="more-projects"><a href="">View More Projects from <?=$page->title?></a></div>
<?endif?>
<?include('foot.php')?>
